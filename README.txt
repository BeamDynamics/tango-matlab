to recompile tango-matlab

>> cd /tango-matlab
>> make -f Makefile.linux


make sure matlab path includes:
/tango-matlab/m-files
/tango-matlab/mex-file

if linux version changes or matlab version changes or both:
    change in Makefile.linux
    
SEGARCH=debian7
MATLAB_BASE=/sware/com/matlab_2015b

SEGARCH=debian8
MATLAB_BASE=/sware/com/matlab_2018b

>> cd /tango-matlab
>> make -f Makefile.linux clean

Define the environment variable LD_LIBRARY_PATH in your .bashrc file: 
export LD_LIBRARY_PATH=/operation/dserver/debian9/lib
