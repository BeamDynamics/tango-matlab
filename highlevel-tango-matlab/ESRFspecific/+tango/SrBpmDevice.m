classdef SrBpmDevice < tango.Device
    % EbsBpmDevice	Interface to the Libera-All device
    %
    % EbsBpmDevice(name) builds an object connected to the device "name"
    % EbsBpmDevice()     uses the default name SR/D-BPMLIBERA/ALL
    %
    % EbsBpmDevice Properties:
    %   Averaging      Default averaging value (RW)
    %   IgnoreIncoh    Default "ignore incoherent BPMs" flag (RW)
    %   Enabled        Mask of enabled BPM blocks (RO)
    %   DDEnabled	   Mask of blocks enabled for turn-by-turn processing (RW)
    %   MinOscillation Threshold for too small oscillations (in average oscillation units)
    %   MaxOscillation Threshold for too large oscillations (in average oscillation units)
    %   MaxOrbit       Threshold for too large orbit (in std orbit units)
    %
    % EbsBpmDevice Methods:
    %   read_sa     Read SA corrected positions
    %   read_dd     Read DD positions and sum signal
    %   okturns     Identifies turns with beam
    %   okbpms      Identifies valid BPM blocks
    %   cleandata   Checks validity and extracts the closed orbit
    %
    
    properties(Constant,Access=private,Hidden)
        mask_error=uint32(hex2dec('1FF'))
        bit_incoh=8
        bit_disabled=2
    end
    properties(Access=private,Hidden)
        mask=tango.SrBpmDevice.mask_error
    end
    properties
        Averaging=1;	% Default averaging value (RW)
        DDEnabled=true(1,320)	% Mask of blocks enabled for turn-by-turn processing (RW)
        MinOscillation=0.25 % Threshold for too small oscillations (in average oscillation units) (RW)
        MaxOscillation=3    % Threshold for too large oscillations (in average oscillation units) (RW)
        MaxOrbit=5  % Threshold for too large orbit (in std orbit units) (RW)
    end
    properties(Dependent=true)
        IgnoreIncoh     % Default "ignore incoherent BPMs" flag (RW)
    end
    properties(Dependent=true,SetAccess=private)
        Enabled         % Mask of enabled BPM blocks (RO)
    end
    
    methods(Static)        
        function [oscillation,orbit]=oscillation(xdata,validturns,last)
            %Extract the static part (closed orbit) and the oscillation
            %
            %[oscillation,orbit]=bpm.oscillation(xdata,validturns,refturn)
            %
            %xdata:         beam positions as returned by bpm.read_dd (nbpms x nturns x nave)
            %validturns:	turns with beam (Default: all turns)
            %refturn:       the closed orbit is computed only if all turns
            %               are valid, by averaging the position over turns
            %               1 to refturn. refturn should be either the last
            %               turn or the turn before the beam kick.
            %               Default: last turn
            [nbpms,nturns,nav]=size(xdata);
            if nargin < 3, last=nturns; end
            if nargin < 2
                turnok=true(1,nturns);
            elseif ~islogical(validturns)
                turnok=false(1,nturns);
                turnok(validturns)=true;
            else
                turnok=validturns;
            end
            nok=sum(turnok);
            if all(turnok)
                fprintf('stored beam\n');
                orbit=mean(xdata(:,1:last,:),2,'omitnan');
            else
                fprintf('1st turn: %d\n',find(turnok,1));
                orbit=zeros(nbpms,1,nav);
            end
            oscillation=NaN(size(xdata));
            oscillation(:,turnok,:)=xdata(:,turnok,:)-orbit(:,ones(1,nok),:);
        end
    end
    
    methods
        
        % Constructor
        
        function dev=SrBpmDevice(varargin)
            %bpm=SrBpmDevice(name)
            %bpm=SrBpmDevice() uses the default name 'srdiag/beam-position/all'
            if nargin < 1, varargin{1}='srdiag/bpm/all'; end
            dev=dev@tango.Device(varargin{:});
            dev.DDEnabled=dev.Enabled;
        end
        
        % Property access
        
        function flag=get.IgnoreIncoh(dev)
            flag=~logical(bitget(dev.mask,dev.bit_incoh));
        end
        function set.IgnoreIncoh(dev,value)
            dev.mask=bitset(dev.mask,dev.bit_incoh,~value);
        end
        
        function enabled=get.Enabled(dev)
            enabled=~logical(bitget(typecast(dev.All_Status.read,'uint32'),dev.bit_disabled));
            
        end
        
        function set.Averaging(dev,value)
            if ~(isscalar(value) && isnumeric(value) && (value>0))
                dev.error('RangeError','Date Range error',...
                    'Averaging must be a positive integer').throw()
            end
            dev.Averaging=value;
        end
        
        % Public methods
        
        function [x,z]=read_sa(dev,varargin)
            %Get SA positions (slow acquisition)
            %
            %[x,z]=bpm.read_sa()
            %   get horizontal and vertical positions using
            %   the default averaging values and incoherence treatment
            %
            %[x,z]=bpm.read_sa(...,'Averaging',n)
            %   waits for n measurements(seconds) and returns the average
            %
            %[x,z]=bpm.read_sa(...,'IgnoreIncoh',true)
            %   ignores the incoherence flag
            %
            %[x,z]=bpm.read_sa(...,'Wait',true|false)
            %   waits for 1 second before making the measurement
            %
            %x,z are 320x 1 arrays
            
            options=struct(varargin{:});
            try
                avg=options.Averaging;
            catch
                avg=dev.Averaging;
            end
            try
                msk=bitset(dev.mask,dev.bit_incoh,~options.IgnoreIncoherence);
            catch
                msk=dev.mask;
            end
            ww=double(isfield(options,'Wait') && options.Wait);
            xz=zeros(320,2);
            nb=zeros(320,1);
            for i=1:avg
                pause(ww);
                v=dev.get_attribute('All_Status','SA_VPositions','SA_HPositions');
                %%%XXX v=dev.get_attribute('All_Status','XZ_Position_SA_corrected');
                ok=(bitand(typecast(v(1).read,'uint32'),msk) == 0);
                xz(ok,:)=xz(ok,:)+[v(2).read(:,ok)' v(3).read(:,ok)'];
                nb(ok)=nb(ok)+1;
                ww=1;
            end
            x=xz(:,1)./nb;
            z=xz(:,2)./nb;
        end
        
        function varargout=read_dd(dev,varargin)
            %Get turn-by-turn data (position or sum signal)
            %
            %data=read_dd()
            %   get turn-by-turn intensity (sum of 4 electrodes)
            %
            %data=read_dd(plane)
            %   get turn-by-turn position or intensity
            %   plane: h|H|x|X|1, v|V|z|Z|2 or s|S|0
            %
            %[data1,data2,...]=read_dd(plane1,plane2,...)
            %   get turn-by-turn data in several planes
            %
            %[...]=bpm.read_dd(...,kicker)
            %   First send a "On" command on "kicker" and wait for all
            %   libera counters to be incremented by 1
            %
            %[...]=bpm.read_dd(...,trigger_func)
            %   First call trigger_func() and wait for all libera counters
            %   to be incremented by 1
            %
            %[...]=bpm.read_dd(...,'Averaging',n)
            %   waits for n measurements, checks that there is no repeated
            %   data and and returns all the values in a
            %  320 x nturns x n arrays (not the average)
            %
            %[...]=bpm.read_dd(...,'SleepTime',t)
            %   Wait for SleepTime before accessing the data [default: 0.3 s]
            %
            %[...]=bpm.read_dd(...,'timeout',tm)
            %   waits a maximum of tm seconds for TriggerCount to be incremented
            %
            %data is a 320 x nturns x n_average array
            
            options={'s'};
            options(1:length(varargin))=varargin;
            istrigger=cellfun(@(arg) isa(arg,'function_handle') || isa(arg,'tango.Device'),options);
            trigger=cellfun(@settrig, options(istrigger),'UniformOutput', false);
            options=options(~istrigger);
            [avg,options]=cs.getoption(options,'Averaging',dev.Averaging);
            [sleeptime,options]=cs.getoption(options,'SleepTime',0.3);
            [timeout,options]=cs.getoption(options,'timeout',[]);
            attrnames=cellfun(@(arg) cs.planeid(arg,'Choices',...
                {{'x','h'},{'z','v'},'s'},...
                {'TBT_HPosition_Img','TBT_VPosition_Img','TBT_Sum_Img'}),...
                options,'UniformOutput',false);
            
            dt=cell(1,avg);
            if ~isempty(trigger) || ~isempty(timeout) || (avg > 1)
                if isempty(timeout), timeout = 3; end
                cnt0=dev.TriggerCounters.read;
                %%%%xxx cnt0=dev.All_DD_TriggerCounter.read;
                tmax=2;
            else
                tmax=0;
            end
            for i=1:avg
                ok=true;
                for test=1:tmax     % try tmax times
                    cellfun(@(fun) fun(), trigger);
                    cnt=0;
                    ok=dev.check(@testcnt,{0,timeout,0.25});
                    
                    cnt0=cnt;
                    if ok
                        pause(sleeptime);   % Wait after counters are incremented
                        break;              % before reading the buffers
                    end
                end
                if ~ok
                    dev.error('CheckTimeout','Check Timeout','Timeout evaluating the condition').throw();
                end
                val=dev.get_attribute(attrnames{:});
                bad=arrayfun(@(attr) attr.quality~=tango.AttrQuality.VALID, val);
                if any(bad)
                    val(find(bad,1)).error.throw()
                end
                dt{i}={val.read};
            end
            varargout=cellfun(@pack_and_check,num2cell(cat(1,dt{:}),1),'UniformOutput',false);
            
            function trig=settrig(trig)
                if isa(trig,'tango.Device')
                    trig=trig.On;
                end
            end
            function ok=testcnt(d)
                cnt=d.TriggerCounters.read;
                ok=all(cnt>cnt0);
            end
            function data=pack_and_check(cl)
                data=cat(3,cl{:});
                db=squeeze(all(data(:,:,1:end-1)==data(:,:,2:end),2));
                if any(any(db))
                    dev.error('DuplicateValue','Duplicated values',...
                        'Duplicated values between data sets').throw();
                end
            end
        end
        
        function ok=okturns(dev,idata,threshold)
            %ok=bpm.okturns(sumdata,threshold)
            %
            %Identifies the turns where the sum of beam intensities from
            %all the validated BPM blocks is greater than
            %its maximum value * threshold
            %
            %threshold: Default 0.5
            if nargin<3, threshold=0.5; end
            intave=sum(sum(idata(dev.DDEnabled,:,:),1),3);
            intmax=max(intave);
            ok=(intave>threshold*intmax);
        end
        
        function ok=okbpms(dev,oscillation,orbit)
            %ok=bpm.okbpms(oscillation,orbit)
            %
            %Identifies valid BPM blocks by eliminating
            %- user-disabled blocks
            %- blocks where the orbit is more than MaxOrbit*rms value
            %- blocks where the oscillation is less that MinOscillation x average
            %- blocks where the oscillation is more than MaxOscillation x average
            [nbpms,~,nav]=size(oscillation);
            ok=true(nbpms,1,nav);
            
            dispn('eliminated:', ~dev.DDEnabled);
            ok(~dev.DDEnabled,1,:)=false;       % remove user choice
            
            %missing=~all(isfinite([oscillation orbit]),2);
            missing=all(~isfinite([oscillation orbit]),2);
            dispn('NaN values:', missing);
            ok(missing)=false;                  % remove BPMs with NaNs
            
            orbit(~ok)=NaN;
            for i=1:3
                rmsorbit=std(orbit(ok),'omitnan');  % single orbit for all
                if rmsorbit < 50e-6, rmsorbit=50e-6; end
                outave=abs(orbit)>dev.MaxOrbit*rmsorbit;
                ok(outave)=false;               % remove orbit exceptions
            end
            dispn('orbit too large:',outave);
            
            ampl=std(oscillation,0,2,'omitnan');% Safe for nturns=1
            ampl(~ok)=NaN;
            rmsampl=mean(reshape(ampl,14,16,nav),2,'omitnan');
            if min(rmsampl) >= 0.00010
                rmsampl=reshape(rmsampl(:,ones(1,16),:),nbpms,1,nav);
                outrms=ampl < dev.MinOscillation*rmsampl;
                ok(outrms)=false;               % remove fixed BPMs
                dispn('oscillation too small:', outrms);
                outrms=ampl > dev.MaxOscillation*rmsampl;
                ok(outrms)=false;               % remove large oscillations
                dispn('oscillation too large:', outrms);
            end
            
            function dispn(code, mask)
                if islogical(mask), mask=find(any(mask,3)); end
                disp([num2str(length(mask)) ' ' code]);
                for wb=reshape(mask,1,[])
                    [bname,kdx]=ebs.bpmname(wb);
                    fprintf('     %s(%i)\n',bname,kdx);
                end
            end
        end
        
        function [osc,orb]=cleandata(dev,val,varargin)
            %Validity check and extraction of the closed orbit
            %
            %[oscillation,orbit]=bpm.cleandata(xdata,validturns,refturn)
            %
            %Identifies valid BPM blocks by eliminating
            %- user-disabled blocks
            %- blocks where the orbit is more than 4*rms value
            %- blocks where the oscillation is less that 0.25 x average
            %- blocks where the oscillation is more than 3 x average
            %
            %xdata:         beam positions as returned by bpm.read_dd (nbpms x nturns x nave)
            %validturns:	turns with beam (Default: all turns)
            %refturn:       the closed orbit is computed only if all turns
            %               are valid, by averaging the position over turns
            %               1 to refturn. refturn should be either the last
            %               turn or the turn before the beam kick.
            %               Default: last turn
            %
            %orbit:         Average position (closed orbit)
            %oscillation:   Turn-by-turn position with average removed
            [osc,orb]=dev.oscillation(val,varargin{:});
            ok=dev.okbpms(osc,orb);
            osc(~ok(:,ones(1,size(osc,2)),:))=NaN;
            orb(~ok)=NaN;
        end
        
        % Compatibility functions
        
        function [x,z,error]=dvreadbpm(dev,waitflag)
            %Compatibility function: read and check BPM positions
            try
                [x,z]=dev.read_sa('Wait',nargin>=2 && strcmpi(waitflag,'Wait'));
                err=0;
            catch err
                if ~isa(err,'tango.Error'), err.rethrow(); end
                x=[];
                z=[];
            end
            if nargout>=3, error=err; end
        end
        function error=dvsetparam(dev,param,value)
            %Compatibility function: set parameters ('BpmAverage','BpmIgnoreIncoherence')
            try
                switch param
                    case 'BpmAverage'
                        dev.Averaging=value;
                    case 'BpmIgnoreIncoherence'
                        dev.CheckIncoh=value;
                end
                err=0;
            catch err
                if ~isa(err,'tango.Error'), err.rethrow(); end
            end
            if nargout>=1, error=err; end
        end
    end
    
end

