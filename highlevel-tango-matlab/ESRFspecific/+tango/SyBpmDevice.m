classdef SyBpmDevice < tango.Device
    % SyBpmDevice	Interface to the Booster Liberasp-All device
    %
    % SyBpmDevice(name) builds an object connected to the device "name"
    % SyBpmDevice()     uses the default name SY/D-BPMLIBERA-SP/ALL
    %
    % SyBpmDevice Properties:
    %   Averaging   Default averaging value (RW)
    %   Enabled     Mask of enabled BPM blocks (RO)
    %
    % SyBpmDevice Methods:
    %   read_xz     Read average positions
    %   read_dd     Read turn-by-turn data
    %
    
    properties(Constant,Access=private,Hidden)
        mask_error=uint32(hex2dec('1FF'))
        bit_disabled=2
        %       rotate=[53:75 1:52];
        gx=struct('atid',1,'attr','All_TBT_Positions','id',1);
        gz=struct('atid',1,'attr','All_TBT_Positions','id',2);
        gs=struct('atid',2,'attr','All_TBT_QSUM','id',2);
        gq=struct('atid',2,'attr','All_TBT_QSUM','id',1);
        seld={tango.SyBpmDevice.gx,tango.SyBpmDevice.gz,...
            tango.SyBpmDevice.gs,tango.SyBpmDevice.gq};
    end
    properties(Access=private,Hidden)
        mask=tango.SyBpmDevice.mask_error
    end
    properties
        Averaging=1;    % Default averaging value (RW)
    end
    properties(Dependent=true,SetAccess=private)
        Enabled         % Mask of enabled BPM blocks (RO)
    end
    
    methods
        
        % Constructor
        
        function dev=SyBpmDevice(varargin)
            %bpm=SyBpmDevice(name)
            %bpm=SyBpmDevice() uses the default name "SY/D-BPMLIBERA-SP/ALL"
            if nargin < 1, varargin{1}='sy/d-bpmlibera-sp/all'; end
            dev=dev@tango.Device(varargin{:});
        end
        
        % Property access
        
        function enabled=get.Enabled(dev)
            enabled=~logical(bitget(dev.All_Status.read,dev.bit_disabled));
        end
        
        function set.Averaging(dev,value)
            if ~(isscalar(value) && isnumeric(value) && (value>0))
                dev.error('RangeError','Date Range error',...
                    'Averaging must be a positive integer').throw()
            end
            dev.Averaging=value;
        end
        
        % Public methods
        
        function [x,z]=read_xz(dev,varargin)
            %[x,z]=bpm.read_xz()
            %   get horizontal and vertical positions and BPM status using
            %   the default averaging values and incoherence treatment
            %
            %[x,z]=bpm.read_xz(...,'Averaging',n)
            %   waits for n measurements(seconds) and returns the average
            %
            %[x,z]=bpm.read_xz(...,'Wait',true|false)
            %   waits for the global fillcounter to be incremented
            
            options=struct(varargin{:});
            [avg,options]=cs.getoption(options,'Averaging',dev.Averaging);
            [wt,options]=cs.getoption(options,'Wait',false);
            xz=zeros(75,2);
            nb=zeros(75,1);
            if wt
                cnt0=dev.get_attribute('FillCounter').read;
            else
                cnt0=uint32(0);
            end
            for av=1:avg
                dev.check(@testcnt,{0,4,0.25});
                cnt0=cnt;
                v=dev.get_attribute('All_Status', 'XPosition', 'ZPosition');
                ok=(bitand(typecast(v(1).read,'uint32'),dev.mask) == 0);
                r=[v(2).read;v(3).read];
                xz(ok,:)=xz(ok,:)+r(:,ok)';
                nb(ok)=nb(ok)+1;
                %fprintf('counter: %d, average: %d\n',cnt,av);
            end
            xz=xz./nb(:,[1 1]);
            x=xz(:,1);
            z=xz(:,2);
            function ok=testcnt(d)
                cnt=d.get_attribute('FillCounter').read;
                ok=(cnt>cnt0);
            end
        end
        function varargout=read_dd(dev,varargin)
            %data=read_dd(plane)
            %   get turn-by-turn position, sum or Q
            %   plane: h|H|x|X|1, v|V|z|Z|2, s|S|3 or q|Q|4
            %
            %[d1,d2,...]=read_dd(plane1,plane2,...},...)
            %   get turn-by-turn data in several planes
            %   Default: 's' for sumS
            %
            %[...]=bpm.read_dd(...,'Wait',true|false)
            %   waits for the global fillcounter to be incremented
            %
            %[...]=bpm.read_dd(...,'Averaging',n)
            %   waits for n measurements and returns all the values in
            %   224 x nturns x n arrays (not the average)
            %
            %data: 75 x nturns x n_average array
            
            options={'s'};
            options(1:length(varargin))=varargin;
            [avg,options]=cs.getoption(options,'Averaging',dev.Averaging);
            [wt,options]=cs.getoption(options,'Wait',false);
            planes=cellfun(@(arg) cs.planeid(arg,'Choices',...
                {{'x','h'},{'z','v'},'s','q'},dev.seld),options);
            atids=[planes.atid];
            if wt
                cnt0=dev.get_attribute('FillCounter').read;
            else
                cnt0=uint32(0);
            end
            dt=cell(1,avg);
            for av=1:avg
                dev.check(@testcnt,{0,4,0.25});
                cnt0=cnt;
                if any(atids==1)
                    v=dev.All_TBT_Positions;
                    res{1}=reshape(v.read',[],75,2);
                end
                if any(atids==2)
                    v=dev.All_TBT_QSUM;
                    res{2}=reshape(v.read',[],75,2);
                end
                dt{av}=arrayfun(@(pl) res{pl.atid}(:,:,pl.id)',planes,...
                    'UniformOutput',false);
                %fprintf('counter: %d, average: %d\n',cnt,av);
            end
            varargout=cellfun(@(cl) cat(3,cl{:}),num2cell(cat(1,dt{:}),1),'UniformOutput',false);
            function ok=testcnt(d)
                cnt=d.get_attribute('FillCounter').read;
                ok=(cnt>cnt0);
            end
        end
    end
end

