#------------------------------------------------------------------------------
# This file is part of the Tango binding for Matlab
#------------------------------------------------------------------------------
#
# Copyright (C) 2002-2014  Nicolas Leclercq, Synchrotron SOLEIL. 
#
# The Libera Tango Device is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# The Libera Tango Device is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
#
# Contact: Nicolas Leclercq - Synchrotron SOLEIL
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# OMNIORB BASE DIR
#------------------------------------------------------------------------------
OMNIORB_BASE = /opt/local

#------------------------------------------------------------------------------
# TANGO BASE DIR
#------------------------------------------------------------------------------
TANGO_BASE = /usr/local

#------------------------------------------------------------------------------
# LOG4TANGO BASE DIR
#------------------------------------------------------------------------------
LOG4TANGO_BASE = /usr/local

#------------------------------------------------------------------------------
# MATLAB BASE DIR
#------------------------------------------------------------------------------
#MATLAB_BASE = /Applications/MATLAB_R2015b.app
#MATLAB_BASE = /Volumes/LFPApplications/MATLAB_R2013a.app
#MATLAB_BASE = /Volumes/LFPApplications/MATLAB_R2014a.app
MATLAB_BASE = /Volumes/LFPApplications/MATLAB_R2015b.app

#------------------------------------------------------------------------------
# INCLUDE DIRS
#------------------------------------------------------------------------------
INCLUDE_DIRS = -I. \
               -I$(OMNIORB_BASE)/include \
               -I$(TANGO_BASE)/include/tango \
               -I$(MATLAB_BASE)/extern/include
           
#------------------------------------------------------------------------------
# LIB DIRS
#------------------------------------------------------------------------------
LIB_DIRS = -L$(OMNIORB_BASE)/lib \
           -L$(TANGO_BASE)/lib

#------------------------------------------------------------------------------
# COMPILER/LINKER OPTIONS
#------------------------------------------------------------------------------
# COMMON
#------------------------------------------------------------------------------
MEX_NAME = tango_binding

#------------------------------------------------------------------------------
# GNU/LINUX (TESTED ON RHE-LINUX with GCC-3.4.4 & MAC OS X 10.6.2 with GCC 4.2.1)
#------------------------------------------------------------------------------
CC = $(MATLAB_BASE)/bin/mex
CXX = $(MATLAB_BASE)/bin/mex
#------------------------------------------------------------------------------
#CFLAGS  = -fPIC -W -Wall -Wno-unused -Wno-comment -Wno-sign-compare -fno-enforce-eh-specs
CFLAGS  = -D_REENTRANT -DOMNI_UNLOADABLE_STUBS
CFLAGS += -largeArrayDims
CFLAGS += CXXFLAGS='$$CXXFLAGS -Wno-unused'
CFLAGS += -DMAP_DEV_BOOLEAN_TO_MATLAB_LOGICAL
#------------------------------------------------------------------------------
LD = $(CXX)
#------------------------------------------------------------------------------
#LDFLAGS = LD='xcrun clang++' -cxx -largeArrayDims
LDFLAGS = -cxx -largeArrayDims
#------------------------------------------------------------------------------
MEX_EXT = $(shell $(MATLAB_BASE)/bin/mexext)
#------------------------------------------------------------------------------
#
# add -g and -D_DEBUG to the CFLAGS in case you want compile with debugging information
#
#CFLAGS += -g -D_DEBUG
#
# CFLAGS += -O2
#
#------------------------------------------------------------------------------
#
# add -D_MATLAB_R12_ to the CFLAGS in case your are compiling this binding with Matlab <= R12. 
# 
# CFLAGS += -D_MATLAB_R12_
#
#------------------------------------------------------------------------------
#
# add -D_HAS_NO_MWSIZE_TYPE_ to the CFLAGS in case your are compiling this binding 
# with a Matlab release which does not define the mwSize type (<= 2.7.0.294) 
# 
# CFLAGS += -D_HAS_NO_MWSIZE_TYPE_
#
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# OBJ FILES
#------------------------------------------------------------------------------
OBJS_SO = main.o \
	MexArgs.o \
	MexFile.o \
	MexUtils.o \
	GroupRepository.o \
	DevRepository.o \
	DataAdapter.o \
	TangoBinding.o
	

#------------------------------------------------------------------------------
# SHARED_LIBS
#------------------------------------------------------------------------------
SHARED_LIBS = \
	-ltango \
	-llog4tango \
	-lzmq \
	-lomniORB4 \
	-lomniDynamic4 \
	-lCOS4 \
	-lomnithread \
	-lpthread \
	-lm \
	-ldl

#------------------------------------------------------------------------------
# LIBS
#------------------------------------------------------------------------------
LIBS = $(SHARED_LIBS)

#------------------------------------------------------------------------------
# RULE: all
#------------------------------------------------------------------------------
all: $(MEX_NAME).$(MEX_EXT) install

#------------------------------------------------------------------------------
# RULE: $(MEX_NAME).$(MEX_EXT)
#------------------------------------------------------------------------------
$(MEX_NAME).$(MEX_EXT): $(OBJS_SO)
	$(LD) $(LDFLAGS) -output $(MEX_NAME).$(MEX_EXT) $(OBJS_SO) $(LIB_DIRS) $(LIBS)

#------------------------------------------------------------------------------
# RULE: mexversion.o
#------------------------------------------------------------------------------
#mexversion.o: $(MATLAB_BASE)/extern/src/mexversion.c
#	$(CC) $(CFLAGS) $(INCLUDE_DIRS) $(MATLAB_BASE)/extern/src/mexversion.c -c -o mexversion.o

#------------------------------------------------------------------------------
# RULE: main.o
#------------------------------------------------------------------------------
main.o: main.cpp
	 $(CC) $(CFLAGS) $(INCLUDE_DIRS) main.cpp -c

#------------------------------------------------------------------------------
# RULE: MexArgs.o
#------------------------------------------------------------------------------
MexArgs.o: MexArgs.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) MexArgs.cpp -c

#------------------------------------------------------------------------------
# RULE: MexFile.o
#------------------------------------------------------------------------------
MexFile.o: MexFile.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) MexFile.cpp -c

#------------------------------------------------------------------------------
# RULE: MexUtils.o
#------------------------------------------------------------------------------
MexUtils.o: MexUtils.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) MexUtils.cpp -c

#------------------------------------------------------------------------------
# RULE: DevRepository.o
#------------------------------------------------------------------------------
DevRepository.o: DevRepository.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) DevRepository.cpp -c 

#------------------------------------------------------------------------------
# RULE: GroupRepository.o
#------------------------------------------------------------------------------
GroupRepository.o: GroupRepository.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) GroupRepository.cpp -c

#------------------------------------------------------------------------------
# RULE: DataAdapter.o
#------------------------------------------------------------------------------
DataAdapter.o: DataAdapter.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) DataAdapter.cpp -c

#------------------------------------------------------------------------------
# RULE: TangoBinding.o
#------------------------------------------------------------------------------
TangoBinding.o: TangoBinding.cpp
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) TangoBinding.cpp -c

#------------------------------------------------------------------------------
# RULE: clean
#------------------------------------------------------------------------------
clean:
	rm -rf ./mex-file
	rm -f *.o *~

#------------------------------------------------------------------------------
# RULE: install
#------------------------------------------------------------------------------
install:
	mkdir -p ./mex-file
	cp -f ./$(MEX_NAME).$(MEX_EXT) ./mex-file
	rm -f ./$(MEX_NAME).$(MEX_EXT)








