@ECHO off

:: ======== TANGO HOST 
::set TANGO_HOST=localhost:10000

::------------------------------------------------------------------------
:: ======== MSVC++ 10 ======== 
::------------------------------------------------------------------------
::SET PSDK=C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2
CALL "C:\Program Files\Microsoft Visual Studio 10.0\VC\bin\vcvars32.bat"

::------------------------------------------------------------------------
:: ======== MATLAB PATHS 
::------------------------------------------------------------------------
set ML_ROOT32=C:\Program Files\MATLAB\R2009b\extern
set ML_INC32=%ML_ROOT32%\include
set ML_LIB32=%ML_ROOT32%\lib\win32\microsoft
set ML_RC32=%ML_ROOT32%\include
SET ML_LIB32_LIST=libmx.lib

::------------------------------------------------------------------------
:: ======== LOG4TANGO ======== 
::------------------------------------------------------------------------
SET LOG4TANGO_ROOT32=C:\Soleil\tango-8.0.5-win32-vc10
SET LOG4TANGO_INC32=%LOG4TANGO_ROOT32%\include\vc10
SET LOG4TANGO_LIB32=%LOG4TANGO_ROOT32%\lib\vc10_dll
SET LOG4TANGO_BIN32=%LOG4TANGO_ROOT32%\lib\vc10_dll
SET LOG4TANGO_LIB32_LIST=log4tango.lib
SET PATH=%LOG4TANGO_BIN32%;%PATH%

SET LOG4TANGO_ROOT64=C:\Soleil\tango-8.0.5-x64-vc10
SET LOG4TANGO_INC64=%LOG4TANGO_ROOT64%\include\vc10
SET LOG4TANGO_LIB64=%LOG4TANGO_ROOT64%\lib\vc10_dll
SET LOG4TANGO_BIN64=%LOG4TANGO_ROOT64%\lib\vc10_dll
SET LOG4TANGO_LIB64_LIST=log4tango.lib
SET PATH=%LOG4TANGO_BIN64%;%PATH%

::------------------------------------------------------------------------
:: ======== ZMQ ======== 
::------------------------------------------------------------------------
SET ZMQ_ROOT32=C:\Soleil\tango-8.0.5-win32-vc10
SET ZMQ_INC32=%ZMQ_ROOT32%\include\vc10
SET ZMQ_LIB32=%ZMQ_ROOT32%\lib\vc10_dll
SET ZMQ_BIN32=%ZMQ_ROOT32%\lib\vc10_dll
SET ZMQ_LIB32_LIST=zmq.lib
SET PATH=%ZMQ_BIN32%;%PATH%

SET ZMQ_ROOT64=C:\Soleil\tango-8.0.5-x64-vc10
SET ZMQ_INC64=%ZMQ_ROOT64%\include\vc10
SET ZMQ_LIB64=%ZMQ_ROOT64%\lib\vc10_dll
SET ZMQ_BIN64=%ZMQ_ROOT64%\lib\vc10_dll
SET ZMQ_LIB64_LIST=zmq.lib
SET PATH=%ZMQ_BIN64%;%PATH%

::------------------------------------------------------------------------
:: ======== TANGO ======== 
::------------------------------------------------------------------------
SET TANGO_ROOT32=C:\Soleil\tango-8.0.5-win32-vc10
SET TANGO_INC32=%TANGO_ROOT32%\include\vc10
SET TANGO_LIB32=%TANGO_ROOT32%\lib\vc10_dll
SET TANGO_BIN32=%TANGO_ROOT32%\lib\vc10_dll
SET TANGO_LIB32_LIST=tango.lib
SET PATH=%TANGO_BIN32%;%PATH%

SET TANGO_ROOT64=C:\Soleil\tango-8.0.5-x64-vc10
SET TANGO_INC64=%TANGO_ROO64T%\include\vc10
SET TANGO_LIB64=%TANGO_ROOT64%\lib\vc10_dll
SET TANGO_BIN64=%TANGO_ROOT64%\lib\vc10_dll
SET TANGO_LIB64_LIST=tango.lib
SET PATH=%TANGO_BIN64%;%PATH%

::------------------------------------------------------------------------
:: ======== OMNIORB ======== 
::------------------------------------------------------------------------
SET OMNIORB_ROOT32=C:\Soleil\tango-8.0.5-win32-vc10
SET OMNIORB_INC32=%OMNIORB_ROOT32%\include\vc10
SET OMNIORB_LIB32=%OMNIORB_ROOT32%\lib\vc10_dll
SET OMNIORB_BIN32=%OMNIORB_ROOT32%\lib\vc10_dll
SET OMNIORB_LIB32_LIST=omnithread34_rt.lib;omniORB416_rt.lib;COS416_rtd.lib;omniDynamic416_rt.lib
SET PATH=%OMNIORB_BIN32%;%PATH%

SET OMNIORB_ROOT64=C:\Soleil\tango-8.0.5-x64-vc10
SET OMNIORB_INC64=%OMNIORB_ROOT64%\include\vc10
SET OMNIORB_LIB64=%OMNIORB_ROOT64%\lib\vc10_dll
SET OMNIORB_BIN64=%OMNIORB_ROOT64%\lib\vc10_dll
SET OMNIORB_LIB64_LIST=omnithread34_rt.lib;omniORB416_rt.lib;COS416_rtd.lib;omniDynamic416_rt.lib
SET PATH=%OMNIORB_BIN64%;%PATH%



