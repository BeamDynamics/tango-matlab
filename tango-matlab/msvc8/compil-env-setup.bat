@ECHO off

:: ======== TANGO HOST 
::set TANGO_HOST=localhost:10000

::------------------------------------------------------------------------
:: ======== VC++ 8 ======== 
::------------------------------------------------------------------------
SET PSDK=C:\Program Files\Microsoft Platform SDK for Windows Server 2003 R2
CALL "C:\Program Files\Microsoft Visual Studio 8\VC\bin\vcvars32.bat"

::------------------------------------------------------------------------
:: ======== MAVEN ======== 
::------------------------------------------------------------------------
SET M2_ROOT=C:\Documents and Settings\leclercq\.m2\repository

:: ======== MATLAB PATHS 
set ML_ROOT=C:\Program Files\MATLAB\R2009b\extern
set ML_INC=%ML_ROOT%\include
set ML_LIB=%ML_ROOT%\lib\win32\microsoft
set ML_RC=%ML_ROOT%\include
SET ML_LIB_LIST=libmx.lib

::------------------------------------------------------------------------
:: ======== YAT PATHS ======== 
::------------------------------------------------------------------------
set YAT_ROOT=%M2_ROOT%\fr\soleil\lib\YAT-x86-Windows-msvc-static-release\1.6.3\nar
set YAT_INC=%YAT_ROOT%\include
set YAT_LIB=%YAT_ROOT%\lib\x86-Windows-msvc\static
set YAT_LIB_LIST=YAT-x86-Windows-msvc-static-release-1.6.3.lib

::------------------------------------------------------------------------
:: ======== YAT FOR TANGO PATHS ======== 
::------------------------------------------------------------------------
set YAT_FOR_TANGO_ROOT=%M2_ROOT%\fr\soleil\lib\YAT4Tango-x86-Windows-msvc-static-release\1.5.8\nar
set YAT_FOR_TANGO_INC=%YAT_FOR_TANGO_ROOT%\include
set YAT_FOR_TANGO_LIB=%YAT_FOR_TANGO_ROOT%\lib\x86-Windows-msvc\static
set YAT_FOR_TANGO_LIB_LIST=YAT4Tango-x86-Windows-msvc-static-release-1.5.8.lib

::------------------------------------------------------------------------
:: ======== TANGO ======== 
::------------------------------------------------------------------------
::SET TANGO_ROOT=D:\Projets\soleil-root-tango-7.2.6-win32\tango
SET TANGO_ROOT=C:\Soleil\tango-7.2.6-dist-msvc-8.0.50727.762\tango
SET TANGO_INC=%TANGO_ROOT%\include
SET TANGO_LIB=%TANGO_ROOT%\lib\shared
SET TANGO_BIN=%TANGO_ROOT%\bin
SET TANGO_LIB_LIST=tango726.lib log4tango.lib
SET PATH=%TANGO_BIN%;%PATH%

::------------------------------------------------------------------------
:: ======== OMNIORB ======== 
::------------------------------------------------------------------------
::SET OMNIORB_ROOT=D:\Projets\soleil-root-tango-7.2.6-win32\omniorb
SET OMNIORB_ROOT=C:\Soleil\tango-7.2.6-dist-msvc-8.0.50727.762\omniorb
SET OMNIORB_INC=%OMNIORB_ROOT%\include
SET OMNIORB_LIB=%OMNIORB_ROOT%\lib\shared
SET OMNIORB_BIN=%OMNIORB_ROOT%\bin
SET OMNIORB_LIB_LIST=omnithread34_rt.lib omniORB414_rt.lib COS414_rt.lib omniDynamic414_rt.lib
SET PATH=%OMNIORB_BIN%;%PATH%



